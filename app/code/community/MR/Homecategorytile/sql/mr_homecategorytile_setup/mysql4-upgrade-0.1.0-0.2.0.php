<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

$installer = $this;

$installer->startSetup();
$installer->addAttribute("catalog_category", "home_tile_custom_url",  array(
    "type"     => "varchar",
    "backend"  => "",
    "frontend" => "",
    "label"    => "Custom URL for the home page tile",
    "input"    => "text",
    "class"    => "",
    "source"   => "",
    "global"   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    "visible"  => true,
    "required" => false,
    "user_defined"  => false,
    "default" => "",
    "searchable" => false,
    "filterable" => false,
    "comparable" => false,
    "group"=> "Home page tiles",
    "visible_on_front"  => false,
    "unique"     => false,
    "note"       => ""
));

$entityTypeId     = $installer->getEntityTypeId('catalog_category');
$attributeSetId   = $installer->getDefaultAttributeSetId($entityTypeId);

$bind   = array('attribute_set_id' => $attributeSetId,'attribute_group_name'=>'Home page tiles');
$select = $this->getConnection()->select()
    ->from($this->getTable('eav/attribute_group'), 'attribute_group_id')
    ->where('attribute_set_id = :attribute_set_id')
    ->where('attribute_group_name = :attribute_group_name')
    ->limit(1);

$groupId =  $this->getConnection()->fetchOne($select, $bind);

$installer->addAttributeToGroup(
    $entityTypeId,
    $attributeSetId,
    $groupId,
    'home_tile_image',
    '1'
);

$installer->addAttributeToGroup(
    $entityTypeId,
    $attributeSetId,
    $groupId,
    'is_home_tile',
    '0'
);

$installer->endSetup();
