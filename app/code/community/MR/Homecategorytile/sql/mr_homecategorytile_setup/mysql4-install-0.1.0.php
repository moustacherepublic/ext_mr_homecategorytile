<?php
$installer = $this;
$installer->startSetup();

$installer->addAttribute("catalog_category", "is_home_tile",  array(
    "type"     => "int",
    "backend"  => "",
    "frontend" => "",
    "label"    => "Display Homepage tile",
    "input"    => "select",
    "class"    => "",
    "source"   => "eav/entity_attribute_source_boolean",
    "global"   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    "visible"  => true,
    "required" => false,
    "user_defined"  => false,
    "default" => "",
    "searchable" => false,
    "filterable" => false,
    "comparable" => false,
    "group" => "General Information",
    "visible_on_front"  => false,
    "unique"     => false,
    "note"       => ""

));

$installer->addAttribute("catalog_category", "home_tile_image",  array(
    "type"     => "varchar",
    "backend"  => "catalog/category_attribute_backend_image",
    "frontend" => "",
    "label"    => "Homepage Image",
    "input"    => "image",
    "class"    => "",
    "source"   => "",
    "global"   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    "visible"  => true,
    "required" => false,
    "user_defined"  => false,
    "default" => "",
    "searchable" => false,
    "filterable" => false,
    "comparable" => false,
    "group"=> "General Information",
    "visible_on_front"  => false,
    "unique"     => false,
    "note"       => ""

));


$installer->endSetup();
