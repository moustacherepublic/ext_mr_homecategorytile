<?php


class MR_Homecategorytile_Block_Hometile extends Mage_Core_Block_Template
{
   public function getCategoryTileCollection(){
    $collection = Mage::getModel('catalog/category')->getCollection()
        ->addFieldToFilter('level',2)  //only get top level categories
        ->addFieldToFilter('is_home_tile',1)
        ->addAttributeToSort('position', 'ASC')
        ->addAttributeToSelect('*');
       if(count($collection)){
           return $collection;
       }else{
           return false;
       }
}
}
