<?php
/**
 * Created by PhpStorm.
 * User: tony
 * Date: 27/01/15
 * Time: 3:59 PM
 */
class MR_Homecategorytile_Helper_Data extends Mage_Core_Helper_Abstract {
    public function getCategoryHomeTileImageUrl($category){
        $url = false;
        if ($image = $category->getData('home_tile_image')) {
            $url = Mage::getBaseUrl('media').'catalog/category/'.$image;
        }
        return $url;
    }

    public function getCategoryHomeTileCustomUrl($category){
        if($customUrl = $category->getData('home_tile_custom_url')){
            if(strpos($customUrl,'http') !== false){
                $url = $customUrl;
            }else{
                $prefix = '/';
                if (substr($customUrl, 0, strlen($prefix)) == $prefix) {
                    $customUrl = substr($customUrl, strlen($prefix));
                }
                $url = Mage::getBaseUrl(). $customUrl;
            }
        }else{
            $url = $category->getUrl();
        }
        return $url;
    }
}
